<?php
/**
 * @package   OSEmbed
 * @contact   www.alledia.com, support@alledia.com
 * @copyright 2016 Open Source Training, LLC. All rights reserved
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die();

require_once 'library/Installer/include.php';

use Alledia\Installer\AbstractScript;
use Alledia\Framework\Factory;
use Alledia\OSEmbed\Free\Helper;

/**
 * Custom installer script
 */
class PlgContentOSEmbedInstallerScript extends AbstractScript
{

}
