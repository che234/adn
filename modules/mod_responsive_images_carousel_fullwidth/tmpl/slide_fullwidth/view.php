<?php
/**
* @title		responsive images carousel fullwidth
* @website		http://www.joomhome.com
* @copyright	Copyright (C) 2015 joomhome.com. All rights reserved.
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/

    // no direct access
    defined('_JEXEC') or die;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $mosConfig_live_site; ?>/modules/mod_responsive_images_carousel_fullwidth/tmpl/slide_fullwidth/css/slider.min.css">
<style>
	.res-img-slide-main{
		width:<?php echo $width_module;?>;
		margin:0 auto;
	}
	.res-img-slide-main .full-width .slide img{
		width:100% !important;
	}
	.res-img-slide-main .full-width .inner{
		height:auto !important;
	}
</style>
<div class="res-img-slide-main">
	<div class="full-width">
		<div class="inner">
			<?php
				$count1 =1;
				$real_introtext='';
				foreach($data as $index=>$value)
				{
					if( isset($value['textlimit']) and $value['textlimit']!='no' )
					{
						if( isset($value['introtext']) ) $real_introtext = $helper->textLimit($value['introtext'], $value['limitcount'], $value['textlimit']);
					} else {
						if( isset($value['introtext']) ) $real_introtext = $value['introtext'];
					}
				?>
					<div class="slide">
						<a href="<?php echo $value['link']?>">
							<div><?php echo $real_introtext;?></div>
							<img src="<?php echo JURI::root().$value['image'] ?>" alt="<?php echo $value['title']?>"/>
						</a>
					</div>
			<?php
					$count1++ ; 
				} ?>
		</div>
		<div class="controls">
			<a href="#" class="left">&larr;</a>
			<a href="#" class="right">&rarr;</a>
		</div>
		<div class="slide-nav"></div>
	</div>
</div>
<?php
if ($enable_jQuery == 1) {?>
	<script type="text/javascript" src="<?php echo $mosConfig_live_site; ?>/modules/mod_responsive_images_carousel_fullwidth/tmpl/slide_fullwidth/js/jquery.min.js"></script>
<?php }?>
<script type="text/javascript" src="<?php echo $mosConfig_live_site; ?>/modules/mod_responsive_images_carousel_fullwidth/tmpl/slide_fullwidth/js/slider.js"></script>
<script>
</script>
