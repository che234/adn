<?php

#@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

/* FANCY PANTS ACCORDION */

defined('_JEXEC') or die;

//add stylesheet
$doc = JFactory::getDocument();
$doc->addStyleSheet(JURI::base(true) . '/modules/mod_drawer/assets/css/mod_drawer.css', 'text/css' );

//include the class of the syndicate functions only once
require_once(dirname(__FILE__).'/helper.php');

// if there is a set height then set this variable
$setHeight = $params->get('setHeight');

// if there is a height variable then we apply a style to the head
if ($setHeight != ''){
	$style = '#theDrawer{height:'.$setHeight.'px; overflow:hidden;}';
	$doc->addStyleDeclaration( $style );
}

// if there is a max height then set this variable
$maxHeight = $params->get('maxHeight');

// if there is a max height variable then we apply a style to the head
if ($maxHeight != ''){
	$style = '#theDrawer{max-height:'.$maxHeight.'px; overflow:hidden;}';
	$doc->addStyleDeclaration( $style );
}

if($params->get('modOrArt') == 0){
	// if they've chose a module then we call the module method to get them
	$mods = mod_drawerHelper::return_modules($params);

	// How many modules per row
	// This sets some widths
	$numPerRow = $params->get('howMany');
	switch($numPerRow)
	{
		case '1':
			$style = '#theDrawer .drawerMod{width:98%;}';
			$doc->addStyleDeclaration( $style );
	  		break;
	  	case '2':
	  		$style = '#theDrawer .drawerMod{width:45%;}';
	  		$doc->addStyleDeclaration( $style );
	  		break;
	  	case '3':
	  		$style = '#theDrawer .drawerMod{width:30%;}';
	  		$doc->addStyleDeclaration( $style );
	  		break;
	  	case '4':
	  		$style = '#theDrawer .drawerMod{width:22%;}';
	  		$doc->addStyleDeclaration( $style );
	  		break;
	  	default:

	  		break;
	}


} else if($params->get('modOrArt') == 1){
	// If they've chose an article just grab that
	$arts = mod_drawerHelper::return_article($params);
}

if($params->get('sessionStorage') == 1){
	// if they want to use session storage add a variable definition in the head
	$doc->addScriptDeclaration('
		var persist_the_drawer = true;
	');
}

//keeps module class suffix even if templateer tries to stop it
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

// This method loads in jQuery safely.
mod_drawerHelper::load_jquery($params);

// This loads in the debug / normal script based on the option
// The only difference is debug starts open.
if($params->get('debug') == 0){
	$doc->addScript(JURI::base(true) . '/modules/mod_drawer/assets/js/mod_drawer.min.js');
} else {
	$doc->addScript(JURI::base(true) . '/modules/mod_drawer/assets/js/mod_drawer_debug.min.js');
}

require(JModuleHelper::getLayoutPath('mod_drawer'));
