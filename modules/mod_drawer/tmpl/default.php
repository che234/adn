<?php

	#@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

	defined('_JEXEC') or die;
	// get the button parameters and whether we're showing an article or a module
	$lebutton = $params->get('buttonText');
	$modorart = $params->get('modOrArt');
?>

<div id="theDrawer">

	<?php

		// 0 is if it's a module
		if($modorart == 0){

			jimport( 'joomla.application.module.helper' );

			// we get the module renderer
			$document =& JFactory::getDocument();
			$renderer = $document->loadRenderer('module');

			// make a ul for them to all go in
			echo '<ul class="modulelist">';
    		foreach($mods as $moddy){
				// render out the modules

    			// just to get rid of that stupid php warning
    			$moddy->user = '';
    			$parameters = array('style'=>'xhtml');
    			echo '<li class="drawerMod">'.$renderer->render($moddy, $parameters).'</li>';
    		}
  			echo '</ul>';

		}

		// if it's an article
		if($modorart == 1){

			// get the parameters for the image and generate a url
			$itemID = $arts['id'];
			$url = 'index.php?option=com_content&view=article&id='.$itemID;
			$title = $arts['title'];
			$intro = $arts['introtext'];
			$full = $arts['fulltext'];

			// spit it all into an element
			?><div id="drawerContent"><?php
			echo '<h2>'.$title.'</h2>';
			echo $intro;
			if($full != ''){
				echo "<a href='.$url.'>Read More</a>";
			}
			?></div><?php
		}
	?>

</div>

<div id="theButton">
	<a href="#"><?php echo $lebutton; ?></a>
</div>

<?php
