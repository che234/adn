<?php

#@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

defined('_JEXEC') or die;

jimport('joomla.application.component.model');
JModelLegacy::addIncludePath(JPATH_SITE.'/components/com_content/models');

class mod_drawerHelper{

	public function return_modules(&$params){

		// import the module helper
		jimport( 'joomla.application.module.helper' );
		$pos = $params->get('whatMod');

		// use the database to get modules in the position chosen and load them into an array
		$db =& JFactory::getDBO();
		$sql = "SELECT * FROM #__modules WHERE position = '".$pos."' AND published = 1 ORDER BY ordering";
		$db->setQuery($sql);
		$modules = $db->loadObjectList();

		return $modules;
	}

	public static function return_article(&$params){

		// Get the article chosen. Believe it's an ID
		$zearticle = $params->get('whatArt');

		$db = JFactory::getDBO();

		// Grab the article using that ID
		$sql = "SELECT * FROM #__content WHERE id = ".intval($zearticle);
		$db->setQuery($sql);
		$items = $db->loadAssoc();

		return $items;

	}

	public static function load_jquery(&$params){
		// In Joomla 3 this will use built in methods to get jQuery without conflicting.
		// In Joomla 2 it will check and add it in with a no conflict script too so it doesn't conflict
		// with mootools $ sign.
		if($params->get('load_jquery')){
			JLoader::import( 'joomla.version' );
			$version = new JVersion();
			if (version_compare( $version->RELEASE, '2.5', '<=')) {
					$doc = &JFactory::getDocument();
					$app = &JFactory::getApplication();
					$file='https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js';
					$file2=JURI::root(true).'/modules/mod_drawer/assets/js/noconflict.js';
					$doc->addScript($file);
					$doc->addScript($file2);
			} else {
				JHtml::_('jquery.framework');
			}
		}
	}

}
